

let userSchema = {
    username: String,
    name: String,
    phone: Number,
    email: String,
    address: [],
    password: String,
    range: String,
    state: Boolean,
    ban : Boolean,
    userKey : Number,
    reco: []
};


export default userSchema;

