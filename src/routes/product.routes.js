
import express from "express";
import { delProduct, modProduct, newProduct, showProductDB } from '../middle/product.middle.js'
const router = express.Router();


/**
 * @swagger
 * /user/products:
 *  get:
 *    tags: 
 *      [Product]
 *    summary: Mostrar productos 
 *    description: Muestra todos los productos 
 *      [Product]
 *    parameters:
 *    - name: authorization
 *      description: Token de usuario
 *      in: header
 *      required: false
 *      type: string
 *    responses:
 *      200:
 *        description: Se muestran todos los medios de pago
 *      403:
 *        description: Credenciales incorrectas
 * 
 * 
 */


router.get('/user/products', (req, res) => {
    showProductDB(req,res);
});

/**
 * @swagger
 * /admin/newProduct:
 *  post:
 *    tags: 
 *      [Product]
 *    summary: Crear Producto
 *    description: Crea un producto y lo almacena en la Base de Datos
 *    parameters:
 *    - name: authorization
 *      description: Token de admin
 *      in: header
 *      required: false
 *      type: string
 *    - name: name
 *      description: Nombre del producto
 *      in: formData
 *      required: false
 *      type: string
 *    - name: subname
 *      description: Nombre abreviado del producto
 *      in: formData
 *      required: false
 *      type: string
 *    - name: price
 *      description: Precio del producto
 *      in: formData
 *      required: false
 *      type: integer
 *    responses:
 *            200:
 *                description: Producto creado exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            400:
 *                description: Error al validar los datos 
 *            409:
 *                description: El nombre de producto y/o su abrevibiación ya existen
 */

 router.post('/admin/newProduct', (req, res) => {
    newProduct(req,res);
});

/**
 * @swagger
 * /admin/modProduct/:
 *  put:
 *    tags: 
 *      [Product]
 *    summary: Modificar datos de un Producto
 *    description: Modifica los datos de un producto
 *    parameters:
 *    - name: authorization
 *      description: Token de admin
 *      in: header
 *      required: false
 *      type: string
 *    - name: id
 *      description: Id del producto a cambiar
 *      in: query
 *      required: false
 *      type: string
 *    - name: name
 *      description: Nombre del producto
 *      in: formData
 *      required: false
 *      type: string
 *    - name: subname
 *      description: Nombre abreviado del producto
 *      in: formData
 *      required: false
 *      type: string
 *    - name: price
 *      description: Precio del producto
 *      in: formData
 *      required: false
 *      type: integer
 *    responses:
 *            200:
 *                description: Producto modificado exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            404:
 *                description: Producto no encontrado
 *            400:
 *                description: Error al validar los datos 
 *            409:
 *                description: El nombre de producto y/o su abrevibiación ya existen
 * 
 */


 router.put("/admin/modProduct/", (req, res) => {
    modProduct(req,res);
});

/**
 * @swagger
 * /admin/deleteProduct/:
 *  delete:
 *    tags: 
 *      [Product]
 *    summary: Eliminar Producto
 *    description: Elimina un producto de la Base de Datos
 *    parameters:
 *    - name: authorization
 *      description: Token de admin
 *      in: header
 *      required: false
 *      type: string
 *    - name: id
 *      description: Id del producto a eliminar
 *      in: query
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Producto eliminado exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            404:
 *                description: Producto no encontrado
 *            400:
 *                description: Error al validar los datos 
 */


 router.delete("/admin/deleteProduct/", (req, res) => {
    delProduct(req,res);
});

export { router }
