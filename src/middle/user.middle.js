// const userControl = require('../controllers/user.controller')
// const validate = require('../controllers/dataVerify');
// const db = require('../config/conexion.config');

import { validateUserData } from '../controllers/dataVerify.js';
import {
    addAddress, ban, changeRights, createUser, dataUser, deleteAddress, deleteUser,
    logOutUser, modifyAddress, modifyAUser, showAllAddress, usersName, validateAdmin, validateUser
} from '../controllers/user.controller.js'
import { User } from '../config/conexion.config.js';



//Show a collection from the Data Base
const showDB = (req, res, bd) => {

    let desToken = validateAdmin(req.headers.authorization);

    if (desToken != false) {
        bd.find()
            .then(resolve => res.status(200).send(resolve))
            .catch(err => res.status(404).send(err));
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

//Show one element of the Data Base (use ID)
const showOneDB = (req, res, bd, id) => {
    bd.find({ _id: id })
        .then(resolve => res.status(200).send(resolve))
        .catch(err => res.status(404).send('No encontrado'));
}




//Create a new user
const newUser = (req, res) => {

    let user = validateUserData(req.body, '', 'new')



    if (user.boolean == true) {
        createUser(user.data)
            .then(resolve => res.status(200).send(resolve))
            .catch(err => res.status(409).send(err));
    } else {
        res.status(400).send('Lo siento.Has enviado datos invalidos.');
    }
}



//Login a new user and create a new user token
const logIn = async (req, res) => {

    let user = validateUserData(req.body, '', 'login')

    if (user.boolean == true) {
        let userAuth;
        
        userAuth = await validateUser(req.body);

        if (userAuth.boolean != false) {
            res.status(200).send(userAuth.token);
        } else {
            res.status(403).send('Las credenciales ingresadas no corresponden a un usuario con acceso');
        }
    } else {
        res.status(400).send('Lo siento.Has enviado datos invalidos.');
    }
}




//Logout an user
const logOut = async (req, res) => {


    let desToken = validateAdmin(req.headers.authorization);


    if (desToken != false) {
        let response = await logOutUser(desToken._id);
        res.status(response.status).send(response.txt)

    } else {
        res.status(403).send('Token invalido o expirado');
    }
}


//Show all connected users
const showUsers = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);

    if (desToken != false) {
        if (desToken.range == 'admin') {
            User.find({ state: true })
                .then(resolve => res.status(200).send(usersName(resolve)))
                .catch(err => res.status(404).send(err));
        } else {
            res.status(403).send('Lo siento, no tienes permiso para acceder a este contenido.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

//Show user data
const showDataUser = async (req, res) => {
    let desToken = validateAdmin(req.headers.authorization);
    if (desToken != false) {
        let response = await dataUser(desToken._id);
        res.status(response.status).send(response.txt)
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

//Modify a user
const modUser = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);


    if (desToken != false) {
        let user = validateUserData(req.body, '', 'update')
        let validator = user.phone || user.username || user.name || user.email || user.address || user.password;
        if (validator != false) {
            let response = await modifyAUser(user, desToken._id);
            res.status(response.status).send(response.txt)
        } else {
            res.status(400).send('Lo siento.Has enviado datos invalidos.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

//Modify the rights of a user
const modRights = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);

    let newRange = '';

    (req.query.rights == 'admin' || req.query.rights == 'client')
        ? (newRange = req.query.rights)
        : (newRange = undefined)

    if (desToken != false) {
        let user;
        user = validateUserData(req.body, '', 'update');
        if (user.name != false && newRange != undefined) {
            if (desToken.range == 'admin') {
                let response = await changeRights(user.name, newRange);
                res.status(response.status).send(response.txt);
            } else {
                res.status(403).send('Lo siento, no tienes permiso para acceder a este contenido.');
            }
        } else {
            res.status(400).send('Lo siento.Has enviado datos invalidos.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

//Delete a user
const delUser = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);

    let cleanPassword = validateUserData(req.body, '', 'update');

    if (desToken != false) {
        if (cleanPassword.password != false) {
            let response = await deleteUser(desToken._id, cleanPassword.password);
            res.status(response.status).send(response.txt);
        } else {
            res.status(400).send('Lo siento.Has enviado datos invalidos.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}


//Show all addresses
const showAddress = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);


    if (desToken != false) {
        let response = await showAllAddress(desToken._id);
        res.status(response.status).send(response.txt);
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}



//Add a new address
const newAddress = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);


    if (desToken != false) {
        let user;
        user = validateUserData(req.body, '', 'update');
        if (user.address != false) {
            let response = await addAddress(desToken._id, user.address);
            res.status(response.status).send(response.txt);
        } else {
            res.status(400).send('Lo siento.Has enviado datos invalidos.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

//Modify an address
const modAddress = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);


    if (desToken != false) {
        let user;
        user = validateUserData(req.body, '', 'update');
        if (user.address != false && user.oldAdr != false) {
            let response = await modifyAddress(desToken._id, user.oldAdr, user.address);
            res.status(response.status).send(response.txt);
        } else {
            res.status(400).send('Lo siento.Has enviado datos invalidos.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}


//Delete an address
const delAddress = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);


    if (desToken != false) {
        let user;
        user = validateUserData(req.body, '', 'update');
        if (user.oldAdr != false) {
            let response = await deleteAddress(desToken._id, user.oldAdr);
            res.status(response.status).send(response.txt);
        } else {
            res.status(400).send('Lo siento.Has enviado datos invalidos.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}


//Ban or unban a user
const usersBan = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);
    let banReq = '';


    (req.query.ban == 'banear usuario' || req.query.ban == 'desbanear usuario')
        ? ('')
        : (banReq = undefined)



    if (desToken != false) {
        let user;
        user = validateUserData(req.body, '', 'update');
        if (user.name != false && banReq != undefined) {
            if (desToken.range == 'admin') {
                (req.query.ban == 'banear usuario') ? (banReq = true) : (banReq = false)
                let response = await ban(user.name, banReq);
                res.status(response.status).send(response.txt);
            } else {
                res.status(403).send('Lo siento, no tienes permiso para acceder a este contenido.');
            }
        } else {
            res.status(400).send('Lo siento.Has enviado datos invalidos.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

export {
    showDB,
    showUsers,
    showOneDB,
    showDataUser,
    newUser,
    logIn,
    logOut,
    modUser,
    modRights,
    delUser,
    showAddress,
    newAddress,
    modAddress,
    delAddress,
    usersBan
}