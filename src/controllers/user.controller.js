import "dotenv/config.js";
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt'
import { User } from '../config/conexion.config.js';
import { findByEmail,findByUsername,findById} from "./search.js";


// SECURITY ZONE

//Encrypt password
const cripty = async (data) => {
    let pass = await bcrypt.hash(data, 8);
    return pass;
}

//Decrypt password
const decripty = async (data, old) => {
    let aux = await bcrypt.compare(data, old);
    return aux;
}

//Create token
const newTokensing = async (data) => {
    data._id = data._id.toString();
    const token = jwt.sign(data, process.env.JWT_SECRET, { expiresIn: '24h' });
    return token;

}

//Decrypt token
const consumeToken = (token) => {
    try {
        let cleantoken = token;
        cleantoken = cleantoken.replace('Bearer ', '');
        const detoken = jwt.verify(cleantoken, process.env.JWT_SECRET);
        return detoken;
    } catch (error) {
        return false;
    }
}

//Create a key for the relation order - user
const generateKey = () => {
    let num = Math.floor(Math.random() * 9999999999) + 1;
    return num;
}

//Create an admin if one does not exist
const createFirstAmdin = async ()=>{
    let users = await User.find({range : 'admin'});
    
    if(users.length == 0){
        let passwordEncrypted = await cripty('a1d2m3i4n5');
        const temp = await new User({
            username: 'admin',
            name: '---',
            phone: 0,
            email: 'admin@gmail.com',
            address: '---',
            password: passwordEncrypted,
            range: 'admin',
            state: false,
            ban: false,
            userKey: 0
        }
        );
        const result = await temp.save();
        return result;
    }
}

// SECURITY ZONE

//Create a new user and save it in the Data Base
const createUser = async (data) => {
    
   
    let validator = await findByEmail(User, data.email);
    let validator2 = await findByUsername(User, data.username);


    let msj;

    (validator == false) ? (msj = `El nombre de usuario ya esta en uso`) : ('');
    (validator2 == false) ? (msj = `El email ya esta en uso`) : ('');
    (validator != false && validator2 != false) ? (msj = `El email y el nombre de usuario ya estan en uso`) : ('');


    if (validator == false && validator2 == false) {
        let passwordEncrypted = await cripty(data.password);

        const temp = await new User({
            username: data.username,
            name: data.name,
            phone: data.phone,
            email: data.email,
            address: data.address,
            password: passwordEncrypted,
            range: 'client',
            state: false,
            ban: false,
            userKey: generateKey()
        }
        );
        const result = await temp.save();
        return result;
    } else {
        throw msj;
    }
};

//Show the data of a user
const dataUser = async (id) =>{
    let msj = {
        status: 0,
        txt: '',
    };

    
    let dataUser = await findById(User,id);

    if(dataUser != false){
    dataUser[0].password = undefined;
    dataUser[0].userKey = undefined;
        msj.txt = dataUser[0];
        msj.status = 200;
    }else{
        msj.txt = 'Usuario no encontrado';
        msj.status = 404;
    }
    return msj;
}

// Search a user by email or username and create a new user token
const validateUser = async (data) => {
    let msj = {
        boolean: false,
        token: '',
    };

    (data.name.includes('@') && data.name.includes('.com'))
        ? (msj.boolean = await findByEmail(User, data.name))
        : (msj.boolean = await findByUsername(User, data.name));
    let dataUser = msj.boolean[0];

    if (msj.boolean != false && dataUser.ban == false) {

        msj.boolean = await decripty(data.password, dataUser.password);
        if (msj.boolean != false) {

            const updateState = await User.updateOne({ _id: dataUser._id },
                {
                    $set: {
                        state: true
                    }
                }
            );

            let dataToken = {
                _id: dataUser._id,
                range: dataUser.range,
            }
            await newTokensing(dataToken)
                .then(resolve => msj.token = resolve);
        }
    } else {
        msj.token = ''
        msj.boolean = false;
    }
    return msj;

}

// Decrypt the token and send it
const validateAdmin = (token) => {
    let desToken = consumeToken(token);

    if (desToken != false) {
        return desToken;
    } else {
        return false;
    }
}


//Logout a user
const logOutUser = async (id) => {
    let msj = {
        txt: '',
        status: 0,
    }
    let aux;
    aux = await findById(User, id);

    if (aux != false) {
        const updateState = await User.updateOne({ _id: id },
            {
                $set: {
                    state: false
                }
            }
        );
        msj.txt = 'Hasta la proxima';
        msj.status = 200;
        return msj
    } else {
        msj.txt = 'Las credenciales ingresadas no corresponden a un usuario valido';
        msj.status = 403;
        return msj
    }
}

//Return only the names of the coneccted users
const usersName = (resolve) => {
    let names = 'Usuarios Conectados : ';
    return resolve.reduce((acc,ele)=> `${acc} ${ele.name},`,names)
}

//Modify cero o more attributes of a user
const modifyAUser = async (data, id) => {

    let msj = {
        txt: '',
        status: 409,
    };

    let validator = await findByEmail(User, data.email);
    let validator2 = await findByUsername(User, data.username);

    (validator == false) ? (msj.txt = `El nombre de usuario ya esta en uso`) : ('');
    (validator2 == false) ? (msj.txt = `El email ya esta en uso`) : ('');
    (validator != false && validator2 != false) ? (msj.txt = `El email y el nombre de usuario ya estan en uso`) : ('');

    if (validator == false && validator2 == false) {
        let originalData = await User.findOne({ _id: id });
        const userUpdated = await User.updateMany({ _id: id },
            {
                $set: {
                    username: (data.username == false) ? (originalData.username) : (data.username),
                    name: (data.name == false) ? (originalData.name) : (data.name),
                    email: (data.email == false) ? (originalData.email) : (data.email),
                    password: (data.password == false) ? (originalData.password) : (await cripty(data.password)),
                    phone: (data.phone == false) ? (originalData.phone) : (data.phone)
                }
            }
        );
        msj.txt = 'Los datos solicitados han sido actualizados';
        msj.status = 200;
    }
    return msj;
}


//Change range of a user (admin or client)
const changeRights = async (name, newRange) => {
    let msj = {
        txt: '',
        status: 0,
    }
    let aux;

    (name == false)
        ? (name = '')
        : ('');

    (name.includes('@') && name.includes('.com'))
        ? (aux = await findByEmail(User, name))
        : (aux = await findByUsername(User, name));

    if (aux != false) {
        const updateRange = await User.updateOne({ _id: aux[0]._id },
            {
                $set: {
                    range: newRange
                }
            }
        );
        msj.txt = 'Usuario actualizado exitosamente';
        msj.status = 200;
        return msj
    } else {
        msj.txt = 'Usuario no encontrado o rango invalido';
        msj.status = 404;
        return msj
    }
}

//Delete a user if the password match
const deleteUser = async (id, password) => {
    let msj = {
        txt: '',
        status: 0,
    }
    let aux;
    let dataUser = await findById(User, id);

    (password == false)
        ? (password = '')
        : ('');


    if (dataUser != false) {
        aux = await decripty(password, dataUser[0].password);
        if (aux != false) {
            const userDeleted = await User.deleteOne({ _id: id });
            msj.txt = 'Usuario eliminado exitosamente';
            msj.status = 200;
        } else {
            msj.txt = 'Las credenciales ingresadas no corresponden a un usuario valido';
            msj.status = 403;
        }
    } else {
        msj.txt = 'Las credenciales ingresadas no corresponden a un usuario valido';
        msj.status = 403;
    }

    return msj
}

//Show all addresses
const showAllAddress = async (id, oldAddress) => {
    let msj = {
        txt: '',
        status: 0,
    };
    let addresses = '';

    let dataUser = await findById(User, id);
    if (dataUser != false) {

        for (let i = 0; i < dataUser[0].address.length; i++) {

            addresses = addresses + `Domicilio ${i + 1}: ${dataUser[0].address[i]}\n`;

        }


        msj.txt = addresses;
        msj.status = 200;
    } else {
        msj.txt = 'Usuario no encontrado';
        msj.status = 404;
    }
    return msj
}


//Add a new address
const addAddress = async (id, newAddress) => {
    let msj = {
        txt: '',
        status: 0,
    }
    let dataUser = await findById(User, id);
    let arrAdr = [];

    if (dataUser != false) {

        arrAdr = dataUser[0].address.filter(address => address == newAddress);

        if (arrAdr.length == 0) {

            const addressUpdated = await User.updateOne({ _id: id },
                {
                    $set: {
                        address: (newAddress == false) ? (dataUser[0].address) : (dataUser[0].address.push(newAddress), dataUser[0].address)
                    }
                }
            );
            msj.txt = 'Domicilio agregada con exito';
            msj.status = 200;
        } else {
            msj.txt = 'La domicilio ya existe';
            msj.status = 409;
        }
    } else {
        msj.txt = 'Usuario no encontrado';
        msj.status = 404;
    }

    return msj
}

//Modify an address
const modifyAddress = async (id, oldAddress, newAddress) => {
    let msj = {
        txt: '',
        status: 0,
    };

    let dataUser = await findById(User, id);
    let indexOldAddr;
    let validateNewAddress;
    if (dataUser != false) {


        indexOldAddr = dataUser[0].address[oldAddress - 1];

        validateNewAddress = dataUser[0].address.filter(address => address == newAddress);

        (indexOldAddr == undefined) ? (msj.txt = 'El domicio a cambiar no exite', msj.status = 404) : ('');
        (validateNewAddress.length != 0) ? (msj.txt = 'El nuevo domicilio ya existe', msj.status = 403) : ('');
        (validateNewAddress.length != 0 && indexOldAddr == undefined) ? (msj.txt = 'El nuevo domicilio ya existe y el domicio a cambiar no exite ', msj.status = 409) : ('');


        if (msj.status != 404 && msj.status != 403 && msj.status != 409) {
            dataUser[0].address[oldAddress - 1] = newAddress;

            const addressUpdated = await User.updateOne({ _id: id },
                {
                    $set: {
                        address: dataUser[0].address
                    }
                }
            );
            msj.txt = 'Domicilio cambiado exitosamente';
            msj.status = 200;
        }
    } else {
        msj.txt = 'Usuario no encontrado';
        msj.status = 404;
    }
    return msj
}

//Delete an address 
const deleteAddress = async (id, oldAddress) => {
    let msj = {
        txt: '',
        status: 0,
    };

    let dataUser = await findById(User, id);
    let indexOldAddr;
    let arrAdr
    if (dataUser != false) {


        indexOldAddr = dataUser[0].address[oldAddress - 1];

        (indexOldAddr == undefined) ? (msj.txt = 'El domicio a eleminar no exite', msj.status = 404) : ('');

        if (msj.status != 404) {
            arrAdr = dataUser[0].address.filter(address => address != indexOldAddr);


            const addressUpdated = await User.updateOne({ _id: id },
                {
                    $set: {
                        address: arrAdr
                    }
                }
            );
            msj.txt = 'Domicilio eliminado exitosamente';
            msj.status = 200;

        }
    } else {
        msj.txt = 'Usuario no encontrado';
        msj.status = 404;
    }
    return msj
}

//Ban a user
const ban = async (name, param) => {
    let msj = {
        txt: '',
        status: 0,
    }
    let aux;

    (name == false)
        ? (name = '')
        : ('');


    (name.includes('@') && name.includes('.com'))
        ? (aux = await search.findByEmail(User, name))
        : (aux = await search.findByUsername(User, name));


    if (aux != false) {
        const updateRange = await User.updateOne({ _id: aux[0]._id },
            {
                $set: {
                    ban: param
                }
            }
        );
        (param == true)
            ? (msj.txt = 'Usuario baneado exitosamente')
            : (msj.txt = 'Usuario desbaneado exitosamente');
        msj.status = 200;
        return msj
    } else {
        msj.txt = 'Usuario no encontrado';
        msj.status = 404;
        return msj
    }
}

//DO NOT DELETE, create an administrator if one does not exist
createFirstAmdin()

export{
    validateAdmin,
    createUser,
    validateUser,
    consumeToken,
    logOutUser,
    usersName,
    modifyAUser,
    changeRights,
    deleteUser,
    showAllAddress,
    addAddress,
    modifyAddress,
    deleteAddress,
    ban,
    dataUser
}
