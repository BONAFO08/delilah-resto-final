// const search = require('./search')
// const db = require('../config/conexion.config');
// const productRedis = require('./redis.product');

import { Product } from '../config/conexion.config.js';
import { newCacheProduct,modifyACacheProduct,deleteACacheProduct } from './redis.product.js';
import { findById, findByName, findBySubname } from './search.js';


//Create a new product and save it in the Data Base
const createProduct = async (data) => {

    let validator = await findByName(Product, data.name);
    let validator2 = await findBySubname(Product, data.subname);

    let msj;

    (validator != false) ? (msj = `El nombre de producto ya esta en uso`) : ('');
    (validator2 != false) ? (msj = `La abreviación de prodcuto ya esta en uso`) : ('');
    (validator != false && validator2 != false) ? (msj = `La abreviación y el nombre de producto ya estan en uso`) : ('');


    if (validator == false && validator2 == false) {

        const temp = await new Product({
            name: data.name,
            subname: data.subname,
            price: data.price,
        }
        );
        
        const result = await temp.save();
        newCacheProduct(result);
        return result;
    } else {
        throw msj;
    }
};

//Modify cero o more attributes of a user
const modifyAProduct = async (data, id) => {

    let msj = {
        txt: '',
        status: 409,
    };

    (id == undefined) ? (id = '') : (id = id.toString().trim());

    let validator = await findByName(Product, data.name);
    let validator2 = await findBySubname(Product, data.subname);
    let validator3 = await findById(Product, id);;


    (validator != false) ? (msj.txt = `El nombre de producto ya esta en uso`) : ('');
    (validator2 != false) ? (msj.txt = `La abreviación de prodcuto ya esta en uso`) : ('');
    (validator != false && validator2 != false) ? (msj.txt = `La abreviación y el nombre de producto ya estan en uso`) : ('');
    (validator3 == false) ? (msj.txt = `El Producto no existe`, msj.status = 404) : ('');

    if (validator == false && validator2 == false && validator3 != false) {

        validator3[0].name = (data.name == false) ? (validator3[0].name) : (data.name);
        validator3[0].subname = (data.subname == false) ? (validator3[0].subname) : (data.subname);
        validator3[0].price = (data.price == false) ? (validator3[0].price) : (data.price);

        const productUpdated = await Product.updateMany({ _id: id },
            {
                $set: {
                    name: validator3[0].name,
                    subname: validator3[0].subname,
                    price: validator3[0].price
                }
            }
        );
        
        modifyACacheProduct(validator3[0]);
        msj.txt = 'Los datos solicitados han sido actualizados';
        msj.status = 200;
    }
    return msj;
};

//Delete a product
const deleteProduct = async (id) => {
    msj = {
        txt: '',
        status: 0,
    };

    let validator = await findById(Product, id);;

    if (validator != false) {
        const productDeleted = await Product.deleteOne({ _id: id });
        deleteACacheProduct(id);
        msj.txt = 'Producto eliminado exitosamente';
        msj.status = 200;
    } else {
        msj.txt = 'Producto no encontrado';
        msj.status = 404;
    }
    return msj
};

export{
    createProduct,
    modifyAProduct,
    deleteProduct
};