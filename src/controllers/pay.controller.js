

import { Pay } from "../config/conexion.config.js";
import { findByName, findById } from "./search.js";


//Create a new payment and save it in the Data Base
const createPay = async (data) => {

    let validator = await findByName(Pay, data.name);

    let msj;

    (validator != false) ? (msj = `El medio de pago ya existe`) : ('');

    if (validator == false) {

        const temp = await new Pay({
            name: data.name
        }
        );
        const result = await temp.save();
        return result;
    } else {
        throw msj;
    }
};

//Modify cero o more attributes of a user
const modifyAPay = async (data, id) => {

    let msj = {
        txt: '',
        status: 409,
    };


    let validator = await findByName(Pay, data.name);
    let validator2 = await findById(Pay, id);;


    (validator != false) ? (msj.txt = `El nombre del medio de pago ya esta en uso`) : ('');

    (validator2 == false) ? (msj.txt = `El medio de pago no existe`, msj.status = 404) : ('');

    if (validator == false && validator2 != false) {
        const payUpdated = await Pay.updateOne({ _id: id },
            {
                $set: {
                    name: (data.name == false) ? (validator2[0].name) : (data.name),
                }
            }
        );
        msj.txt = 'Los datos solicitados han sido actualizados';
        msj.status = 200;
    }
    return msj;
};

//Delete a product
const deletePay = async (id) => {
    msj = {
        txt: '',
        status: 0,
    };

    (id == undefined) ? (id = '') : (id = id.toString().trim());

    let validator = await findById(Pay, id);;

    if (validator != false) {
        const payDeleted = await Pay.deleteOne({ _id: id });
        msj.txt = 'Medio de pago eliminado exitosamente';
        msj.status = 200;
    } else {
        msj.txt = 'Medio de pago no encontrado';
        msj.status = 404;
    }
    return msj
};

export {
    createPay,
    modifyAPay,
    deletePay
};