
// Search by ID
const findById = async (bd, id) => {
try {
    let aux;
    let result = await bd.find({ _id: id })
        .then(resolve => {
            (resolve.length == 0) ? (aux = false) : (aux = resolve)
        });
    return aux;
} catch (err) {
    return false;
}
};


// Search by Email
const findByEmail = async (bd, email) => {
    console.log();
    let aux;
    let result = await bd.find({ email: email })
        .then(resolve => {
            (resolve.length == 0) ? (aux = false) : (aux = resolve)
        });
    return aux;
};

// Search by Username
const findByUsername = async (bd, username) => {
    let aux;
    let result = await bd.find({ username: username })
        .then(resolve => {
            (resolve.length == 0) ? (aux = false) : (aux = resolve)
        });
    return aux;
};

// Search by Name
const findByName = async (bd, name) => {
    let aux;
    let result = await bd.find({ name: name })
        .then(resolve => {
            (resolve.length == 0) ? (aux = false) : (aux = resolve)
        });
    return aux;
};

// Search by Subname
const findBySubname = async (bd, subname) => {
    let aux;
    let result = await bd.find({ subname: subname })
        .then(resolve => {
            (resolve.length == 0) ? (aux = false) : (aux = resolve)
        });
    return aux;
};

// Search by userKey
const findByuserKey = async (bd, userKey) => {
    let aux;
    let result = await bd.find({ userKey: userKey })
        .then(resolve => {
            (resolve.length == 0) ? (aux = false) : (aux = resolve)
        });
    return aux;
};

export  {
    findById,
    findByEmail,
    findByUsername,
    findByName,
    findBySubname,
    findByuserKey
}
