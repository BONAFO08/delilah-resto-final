const cleanData = (data) => {
    if (data != undefined) {
        data = data.toString();
        data = data.trim();
        if (data.length != 0) {
            return data;
        } else {
            return false
        }
    } else {
        return false;
    }
}

//Validate that the user data does not have invalid values
const validateUserData = (newData, oldData, param) => {

    newData.phone = parseInt(newData.phone);
    newData.oldAdr = parseInt(newData.oldAdr);
    let msj = {
        boolean: true,
        data: {}
    };

    
    if (newData.id != undefined) {
        (newData.id.includes('{') && newData.id.includes('}'))?(newData.id = undefined):('');
    }

    //User Base Data
    let userSchema = {
        username: '',
        name: '',
        phone: 0,
        email: '',
        address: [],
        password: '',
        range: "client",
        state: false,
        active: true,
        reco: []
    };

    //Cleaning the data
    let userTemp = {
        username: cleanData(newData.username),
        name: cleanData(newData.name),
        email: cleanData(newData.email),
        address: cleanData(newData.address),
        password: cleanData(newData.password),
        id: cleanData(newData.id)
    }

    //Checking the data

    switch (param) {
        case 'new':
            userSchema.username = (userTemp.username == false) ? (msj.boolean = msj.boolean && false) : (userTemp.username);
            userSchema.name = (userTemp.name == false) ? (msj.boolean = msj.boolean && false) : (userTemp.name);
            userSchema.email = (userTemp.email == false) ? (msj.boolean = msj.boolean && false) : (userTemp.email);
            (userTemp.address == false) ? (msj.boolean = msj.boolean && false) : (userSchema.address.push(userTemp.address));
            (typeof (newData.phone) != "number" || isNaN(newData.phone) == true) ? (msj.boolean = msj.boolean && false) : (userSchema.phone = newData.phone)
            userSchema.password = (userTemp.password == false) ? (msj.boolean = msj.boolean && false) : (userTemp.password);
            msj.data = userSchema;
            return msj;

        case 'update':
            //NUEO
            (typeof (newData.oldAdr) != "number" || isNaN(newData.oldAdr) == true) ? (userTemp.oldAdr = false) : (userTemp.oldAdr = newData.oldAdr);
            //
            (typeof (newData.phone) != "number" || isNaN(newData.phone) == true) ? (userTemp.phone = false) : (userTemp.phone = newData.phone);
            return userTemp;

        case 'login':
            newData.name = (userTemp.name == false) ? (msj.boolean = msj.boolean && false) : (userTemp.name);
            newData.password = (userTemp.password == false) ? (msj.boolean = msj.boolean && false) : (userTemp.password);
            msj.data = newData;
            return msj;
    }

}

//Validate that the product data does not have invalid values
const validateProductData = (newData, oldData, param) => {

    newData.price = parseInt(newData.price);

    if (newData.id != undefined) {
        (newData.id.includes('{') && newData.id.includes('}'))?(newData.id = undefined):('');
    }

    let msj = {
        boolean: true,
        data: {}
    };

    //Product Base Data
    let productSchema = {
        name: '',
        subname: '',
        price: 0
    };

    //Cleaning the data
    let productTemp = {
        name: cleanData(newData.name),
        subname: cleanData(newData.subname),
        id: cleanData(newData.id)
    }

    //Checking the data

    switch (param) {
        case 'new':
            productSchema.name = (productTemp.name == false) ? (msj.boolean = msj.boolean && false) : (productTemp.name);
            productSchema.subname = (productTemp.subname == false) ? (msj.boolean = msj.boolean && false) : (productTemp.subname);
            (typeof (newData.price) != "number" || isNaN(newData.price) == true) ? (msj.boolean = msj.boolean && false) : (productSchema.price = newData.price)
            msj.data = productSchema;
            return msj;

        case 'update':
            (typeof (newData.price) != "number" || isNaN(newData.price) == true) ? (productTemp.price = false) : (productTemp.price = newData.price)
            return productTemp

    }
}

//Validate that the payment data does not have invalid values
const validatePayData = (newData, oldData, param) => {

    let msj = {
        boolean: true,
        data: {}
    };

   
    if (newData.id != undefined) {
        (newData.id.includes('{') && newData.id.includes('}'))?(newData.id = undefined):('');
    }

    //Product Base Data
    let paySchema = {
        name: ''
    };

    //Cleaning the data
    let payTemp = {
        name: cleanData(newData.name),
        id: cleanData(newData.id)
    }

    //Checking the data
    if (param == 'new') {
        paySchema.name = (payTemp.name == false) ? (msj.boolean = msj.boolean && false) : (payTemp.name);
        msj.data = paySchema;
        return msj;

        //Filtering the data
    } else if (param == 'update') {
        return payTemp
    }
}

//Converting a String in a Arr with x Objects
const stringToArr = (food) => {
    let arrFood = food;

    arrFood = arrFood.replace(/,{/g, '{');
    arrFood = arrFood.replace(/\n/g, '');
    arrFood = arrFood.replace(/ /g, '');

    let indexSTART;
    let indexEND;
    let objectAux;
    let newArrFood = [];

    for (let i = 0; i != arrFood.length; i) {
        indexSTART = arrFood.indexOf('{');
        indexEND = arrFood.indexOf('}');
        objectAux = arrFood.substring(indexSTART, indexEND + 1);
        arrFood = arrFood.replace(objectAux, '');
        objectAux = JSON.parse(objectAux);
        newArrFood.push(objectAux);
    }
    return newArrFood;
}

//Checking if food is a arr or a string and returning a arr
const cleaningFood = (food) => {
    let arrFood = food;
    if (typeof (food) == "string") {
        arrFood = stringToArr(food);
    }
    return arrFood;
}

//Checking order data
const validateFood = (food) => {
    let arrFood = food;
    let boolean = true;
    if (arrFood != undefined) {
        for (let i = 0; i < arrFood.length; i++) {
            let idClean = cleanData(arrFood[i].id);
            let ammountClean = cleanData(arrFood[i].ammount);
            (typeof (arrFood[i].ammount) != "number" || isNaN(arrFood[i].ammount) == true) ? (ammountClean = false) : ('')

            if ((idClean || false) && (ammountClean || false)) {
                arrFood[i].id = idClean;
                arrFood[i].ammount = Math.floor(Math.abs(ammountClean));
            } else {
                return boolean = false;
            }
        }
    } else {
        boolean = false;
    }
    return (boolean != false) ? (arrFood) : (boolean);
}

//Checking adrress data
const validateAddress = (address) => {
    let boolean = true;
    let selectedAddress = address;
    selectedAddress = cleanData(selectedAddress);

    (selectedAddress != false) ? (selectedAddress = parseInt(selectedAddress)) : (boolean = false);
    (boolean == false || typeof (selectedAddress) != "number" || isNaN(selectedAddress) == true) ? (boolean = false) : ('');


    return (boolean != false) ? (selectedAddress) : (boolean);
}

//Chacking if the stade is validate
const validateOrderStade = (stade) => {
    let validator = stade == 'Confirmado' || stade == 'En preparación' || stade == 'Enviado' || stade == 'Entregado' || stade == 'Cancelado';
    return validator;
}


export {
    validateUserData,
    validateProductData,
    validatePayData,
    cleaningFood,
    validateFood,
    validateAddress,
    validateOrderStade,
}