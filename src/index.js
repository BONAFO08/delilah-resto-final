

const app = express();




import express from "express";
import helmet from "helmet";
import swaggerUI from "swagger-ui-express";
import swaggerJsDoc from "swagger-jsdoc";

import { router as adminRoutes } from "./routes/admin.routes.js";
import { router as checkRoutes } from "./routes/check.routes.js";
import { router as orderRoutes } from "./routes/order.routes.js";
import { router as payRoutes } from "./routes/pay.routes.js";
import { router as productRoutes } from "./routes/product.routes.js";
import { router as userRoutes } from "./routes/user.routes.js";


import { createOrder } from "./controllers/order.controller.js";
const port = 3000;


//settings

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Delilah Resto App',
            version: '2.0.0'
        }
    },
    apis: ['./routes/user.routes.js',
        './routes/admin.routes.js',
        './routes/product.routes.js',
        './routes/pay.routes.js',
        './routes/order.routes.js'],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);

app.use(express.urlencoded({ extended: true }));
app.use(express.json());




app.use('/api-docs',
    swaggerUI.serve,
    swaggerUI.setup(swaggerDocs));








//middle
app.use(helmet())
app.use(adminRoutes)
app.use(checkRoutes)
app.use(orderRoutes)
app.use(userRoutes)
app.use(productRoutes)
app.use(payRoutes)

// app.use(require("./routes/user.routes"));
// app.use(require("./routes/product.routes"));
// app.use(require("./routes/pay.routes"));
// app.use(require("./routes/order.routes"));
// app.use(require("./routes/admin.routes"));
// app.use(require("./routes/check.routes"));

// BORRAR  BORRAR BORRAR  BORRAR BORRAR  BORRAR BORRAR  BORRAR  BORRAR BORRAR  BORRAR 

// const db = require('./config/conexion.config');

import { mongoose } from "./config/conexion.config.js";

const order66 = (db) => {
    db.deleteMany({}, () => { });
}

// BORRAR  BORRAR BORRAR  BORRAR BORRAR  BORRAR BORRAR  BORRAR  BORRAR BORRAR  BORRAR 

const arr = [1, 2, 3, 4, 5];
// arr.map((value,index,array)=> {
//     console.log('value',value);
//     console.log('index',index);
//     console.log('array',array);
// })

// reduce(callbackfn: (previousValue: T, currentValue: T, currentIndex: number, array: T[]) => T): T;
// reduce(callbackfn: (previousValue: T, currentValue: T, currentIndex: number, array: T[]) => T, initialValue: T): T;

// const test = arr.reduce((previousValue, currentValue, currentIndex, array) => 
// { return previousValue + currentValue }
// ,0)

const arr2 = ['1', '2', '3', '4', '5'];
// const test = arr2.reduce((previousValue, currentValue, currentIndex, array) => 
// { return previousValue + currentValue}
// ,{})

// const test = arr2.reduce((previousValue, currentValue, currentIndex, array) => 
// ({...previousValue,[currentValue] : currentValue})
// ,{})


// console.log(test);




const users = [
    {
        id: 0,
        name: 'Nowa',
        phone: 123,
        address: 'Lastation',
    },
    {
        id: 1,
        name: 'Tina',
        phone: 123,
        address: 'Lastation',
    },
    {
        id: 2,
        name: 'Yoshino',
        phone: 123,
        address: 'Lastation',
    },
    {
        id: 3,
        name: 'Kurumi',
        phone: 123,
        address: 'Lastation',
    },
    {
        id: 4,
        name: 'Blanc',
        phone: 123,
        address: 'Lastation',
    },
    {
        id: 5,
        name: 'Vert',
        phone: 123,
        address: 'Lastation',
    },
    {
        id: 6,
        name: 'Nepko',
        phone: 123,
        address: 'Lastation',
    },
]

const extract1 = (resolve) => {
    let names = 'Usuarios Conectados : ';
    for (let i = 0; i < resolve.length; i++) {
        names = names + ' ' + resolve[i].name + ','
    }
    return names
}



app.listen(port, () => {
    console.log(`Server on in port ${port}`);
});

